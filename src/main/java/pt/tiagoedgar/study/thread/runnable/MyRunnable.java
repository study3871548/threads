package pt.tiagoedgar.study.thread.runnable;

import static java.lang.Thread.sleep;

public class MyRunnable implements Runnable{

    private final int threadNumber;
    private final int nTimes;
    private final long sleepMillis;

    private int returnValue;


    public MyRunnable(int threadNumber, int nTimes, long sleepMillis) {
        this.threadNumber = threadNumber;
        this.nTimes = nTimes;
        this.sleepMillis = sleepMillis;
    }

    @Override
    public void run() {
        for(int i=0; i<nTimes; i++){
            System.out.println("Thread ["+threadNumber+"] Hello nº: "+i);

            /* Simulate error.
               - The exception does not affect other threads (including main thread)
            */
            if(threadNumber == 1 && i==2){
                returnValue = -1;
                throw new RuntimeException("Simulating error on thread: " + threadNumber);
            }



            try {
                sleep(sleepMillis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        //simulating a response from a provider
        returnValue = threadNumber + 100;
    }


    public int getThreadNumber() {
        return threadNumber;
    }

    public int getReturnValue() {
        return returnValue;
    }
}
