package pt.tiagoedgar.study.thread;

import pt.tiagoedgar.study.thread.runnable.MyRunnable;

import java.util.HashMap;
import java.util.Map;

public class Main {

    private static final int THREADS = 4;
    private static final int HELLO_TIMES = 5;
    private static final int THREAD_SLEEP = 1000;


    //Just to access the return values
    private static final Map<Thread, MyRunnable> threads = new HashMap<>();


    public static void main(String[] args) {
        System.out.println("Threads begin!");
        startThreads();
        getThreadResults();
        System.out.println("Threads end!");
    }

    private static void startThreads() {
        System.out.println("Parallel Threads");
        for(int i=0; i<THREADS; i++){
            System.out.println("Starting new thread: " + i);
            MyRunnable myRunnable = new MyRunnable(i, HELLO_TIMES, THREAD_SLEEP);
            Thread thread = new Thread(myRunnable);
            thread.start();

            //Just to access the return values
            threads.put(thread, myRunnable);
        }
    }

    private static void getThreadResults() {
        System.out.println("Lets get the results!");
        threads.forEach((thread, myRunnable) -> {
            try {
                //Wait to thread finish
                thread.join();
            } catch (InterruptedException e) {
                System.err.println("Failed to get results! Error: " + e.getLocalizedMessage());
            }
            System.out.println("Thread "+myRunnable.getThreadNumber()+" Return Value: " + myRunnable.getReturnValue());
        });
    }
}
