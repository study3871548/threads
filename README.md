# Threads

Just a simple example how threads work.



# Output Example

Output for params:
```java
private static final int THREADS = 4;
private static final int HELLO_TIMES = 5;
private static final int THREAD_SLEEP = 1000;
```

Output
```
Threads begin!
Parallel Threads
Starting new thread: 0
Starting new thread: 1
Starting new thread: 2
Starting new thread: 3
Lets get the results!
Thread [1] Hello nº: 0
Thread [0] Hello nº: 0
Thread [2] Hello nº: 0
Thread [3] Hello nº: 0
Thread [1] Hello nº: 1
Thread [0] Hello nº: 1
Thread [2] Hello nº: 1
Thread [3] Hello nº: 1
Thread [1] Hello nº: 2
Thread [2] Hello nº: 2
Thread [0] Hello nº: 2
Thread [3] Hello nº: 2
Exception in thread "Thread-1" java.lang.RuntimeException: Simulating error on thread: 1
	at pt.tiagoedgar.study.thread.runnable.MyRunnable.run(MyRunnable.java:30)
	at java.base/java.lang.Thread.run(Thread.java:1583)
Thread [2] Hello nº: 3
Thread [0] Hello nº: 3
Thread [3] Hello nº: 3
Thread [2] Hello nº: 4
Thread [0] Hello nº: 4
Thread [3] Hello nº: 4
Thread 3 Return Value: 103
Thread 1 Return Value: -1
Thread 0 Return Value: 100
Thread 2 Return Value: 102
Threads end!

Process finished with exit code 0
```